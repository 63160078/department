/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.departmentproject;

/**
 *
 * @author Ow
 */
public class TestDeveloper {
    public static void main(String[] args) {
        
        //week 1
        //user1 worked 34 hours 
        //user2 worked 32 hours 
        System.out.println("Week 1");
        
        Developer user1 = new Developer("0001","Pichai Lablai",'1',34);
        System.out.println("UserID : " + user1.getEmpid() + " Salary this week : " + user1.calSalary() + " Summary of Salary : " + user1.calAllSalary());
        System.out.println(user1.getName());
        
        
        System.out.println("");
        
        
        Developer user2 = new Developer("0002","Kongkiat Torjarus",'2',32);
        System.out.println("UserID : " + user2.getEmpid() + " Salary this week : " + user2.calSalary() + " Summary of Salary : " + user2.calAllSalary());
        System.out.println(user2.getName());
        
        
        System.out.println("");
        System.out.println("Week 2");
        
        
        //week2 hours count reset to zero hour
        
        //week2 
        //user1 worked 28 hours
        //user2 worked 26 hours
        
        user1.updateHours(28);
        System.out.println("UserID : " + user1.getEmpid() + " Salary this week : " + user1.calSalary() + " Summary of Salary : "  + user1.calAllSalary());

        user2.updateHours(26);
        System.out.println("UserID : " + user2.getEmpid() + " Salary this week : " + user2.calSalary() + " Summary of Salary : "  + user2.calAllSalary());
        
        System.out.println("");
        System.out.println("Week 3");
        
        
        //week3 hours count reset to zero hour
        
        //week3 
        //user1 worked 30 hours
        //user2 worked 32 hours
        
        user1.updateHours(30);
        System.out.println("UserID : " + user1.getEmpid() + " Salary this week : " + user1.calSalary() + " Summary of Salary : "  + user1.calAllSalary());

        user2.updateHours(32);
        System.out.println("UserID : " + user2.getEmpid() + " Salary this week : " + user2.calSalary() + " Summary of Salary : "  + user2.calAllSalary());
        
        
        
        
    }
    
}
