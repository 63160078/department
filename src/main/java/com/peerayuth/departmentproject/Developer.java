/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.departmentproject;

/**
 *
 * @author Ow
 */
public class Developer {
    private String empid, name;
    private char rank;
    private double hours, salary;
    
    public double sumSalary = 0;
    
    public Developer(String empid,String name,char rank,double hours) {
        this.empid = empid;
        this.name = name;
        this.rank = rank;
        this.hours = hours;
    }
    
    public String getEmpid() {
        return empid;
    }

    public String getName() {
        return name;
    }
    
    public double calSalary() {
        if (rank == '1') {
            salary = 1000*hours;
        } else if (rank == '2') {
            salary = 1500*hours;
        } else if (rank == '3') {
            salary = 1800*hours;
        }
        
        return salary;
    }
    
    public double calAllSalary() {
        return sumSalary +=  salary;   //sumSalary = sumSalary + salary;
    }
    
    public void updateHours(double hours) {
        this.hours = hours;
    }
    
    
    
    
}
